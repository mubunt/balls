 # *balls*, bouncing balls in a Linux xterm-like window.

*balls* is an example of implementing bouncing balls in a text window as xterm-like. We took a very simplistic model, the ball bouncing in a "perfect" way on a "perfect" surface and gravity does not play a role. The program starts with one ball and the user can add more (up to 8) or remove them with the "+" and "-" keys. The 'q' (or 'Q') key is used to quit the application. 

![Example](./README_images/balls-01.png  "Example")

## LICENSE
**balls** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ balls --help
Usage: balls [OPTION]...
Bouncing balls

  -h, --help     Print help and exit
  -V, --version  Print version and exit

Exit: returns a non-zero status if an error is detected.

$ balls --version
balls - Copyright (c) 2021, Michel RIZZO. All Rights Reserved.
balls - Version 1.0.0
$ balls

```
## STRUCTURE OF THE APPLICATION
This section walks you through **balls**'s structure. Once you understand this structure, you will easily find your way around in **balls**'s code base.

``` bash
$ yaTree
./                   # Application level
├── README_images/   # Screenshots for documentation
│   └── balls-01.png # 
├── src/             # Source directory
│   ├── Makefile     # Makefile
│   ├── balls.c      # Main C program
│   └── balls.ggo    # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── COPYING.md       # GNU General Public License markdown file
├── LICENSE.md       # License markdown file
├── Makefile         # Makefile
├── README.md        # ReadMe markdown file
├── RELEASENOTES.md  # Release Notes markdown file
└── VERSION          # Version identification text file

2 directories, 10 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd balls
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd balls
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level). We consider that $BIN_DIR is a part of the PATH.
```

## NOTES
The source code contains 2 implementations: one **multi-threaded**, the other **iterative**. The first is obtained by using the "*-DTHREADS*" compilation option (present as standard in "*src/Makefile*"). 

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.23.
   - *libsodium* library installed (/usr/local/lib and /usr/local/include), version 1.0.18. Refer to https://github.com/jedisct1/libsodium.
- Developped and tested on XUBUNTU 21.04, GCC v10.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***