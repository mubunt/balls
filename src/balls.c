//------------------------------------------------------------------------------
// Copyright (c) 2021, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: balls
// Bouncing balls
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <signal.h>
#include <termcap.h>
#include <termios.h>
#include <stdarg.h>
#include <stdbool.h>
#include <time.h>
#include <pthread.h>
#include <sys/ioctl.h>

#include <sodium.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "balls_cmdline.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define BALL 							"⯃"
#define MAXBALLS						8
#define WAIT							100000

#define GREY							90
#define	RED 							91
#define	GREEN							92
#define	YELLOW							93
#define	BLUE							94
#define	MAGENTA							95
#define	CYAN							96
#define	WHITE							97

#define START_COLOR						"\033[1;%dm"
#define START_DIM_COLOR					"\033[2;%dm"
#define STOP_COLOR						"\033[0m"

#define SAVEINITSCREEN(n)				do { \
											tputs(screeninit, (int)n, putchar); \
											_screeninit = true; \
											fflush(stdout); \
										} while (0)
#define RESTOREINITSCREEN(n)			if (_screeninit) { \
											tputs(screendeinit, (int)n, putchar); \
											fflush(stdout); \
										}
#define CLEAR_ENTIRE_SCREEN()			tputs(clear, (int)nbLINES, putchar)
#define HIDE_CURSOR()					fprintf(stdout, "\033[?25l")
#define SHOW_CURSOR()					fprintf(stdout, "\033[?25h")
#define DISPLAY_AT(line, column, ...)	do { \
											tputs(tgoto(move, ((int)(column - 1) % nbCOLS), (int)(line - 1)), 1, putchar); \
											fprintf(stdout,  __VA_ARGS__); \
											fflush(stdout); \
										} while (0)
#define DISPLAY_BELL()					do { \
											fprintf(stdout, "\033[?5h"); fflush(stdout); \
											usleep(50000); \
											fprintf(stdout, "\033[?5l"); fflush(stdout); \
										} while (0)
//------------------------------------------------------------------------------
// TYPEDEFS & ENUMS
//------------------------------------------------------------------------------
typedef enum { MODE_ECHOED, MODE_RAW }	terminal;
typedef enum { NW = 0, NE, SW, SE }		direction;
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
struct s_ballCoordinates {
	int line;
	int column;
};
struct s_ballSteps {
	int line;
	int column;
};
struct s_ballAttributes {
	struct s_ballSteps steps;
	struct s_ballCoordinates coordinates;
	direction direction;
};
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
#ifdef THREADS
static pthread_t 					threads[MAXBALLS];
static volatile bool		 		cancelThreads[MAXBALLS];
static int							nbBALLS 			= 0;
#else
static volatile int					nbBALLS 			= 0;
#endif
static struct gengetopt_args_info	args_info;
static char 						*screeninit;			// Startup terminal initialization
static char 						*screendeinit;			// Exit terminal de-initialization
static char 						*clear;					// Clear screen
static char 						*move;					// Cursor positioning
static int 							nbLINES				= 0;
static int 							nbCOLS				= 0;
static bool							_screeninit			= false;
static int							colors[ MAXBALLS ]	= { RED, YELLOW, GREEN, BLUE, MAGENTA, CYAN, WHITE, GREY };
static struct s_ballAttributes		balls[ MAXBALLS ];
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void setTerminalMode( terminal );
static void clearBall( int );
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void restoreScreen( void ) {
	SHOW_CURSOR();
	setTerminalMode(MODE_ECHOED);
	RESTOREINITSCREEN(nbLINES);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void fatal( const char *format, ... ) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	restoreScreen();
	fprintf(stderr, "\n" START_COLOR "ERROR: %s. Abort!" STOP_COLOR "\n\n", RED, buff);
	exit(EXIT_FAILURE);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void getTerminalSize( int *lines, int *cols ) {
	struct winsize w;
	if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w))
		fatal("%s", "Cannot get terminal size Abort!");
	*lines = w.ws_row;
	*cols = w.ws_col;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void sigWindowSize( int sig ) {
	signal(SIGWINCH, SIG_IGN);
	int cols, lines;
	getTerminalSize(&lines, &cols);
	if (lines < nbLINES || cols < nbCOLS) {
		for (int i = 0; i < nbBALLS; i++) {
			clearBall(i);
			if (balls[i].coordinates.line > lines)
				balls[i].coordinates.line = lines;
			if (balls[i].coordinates.column > cols)
				balls[i].coordinates.column = cols;
		}
	}
	nbLINES = lines;
	nbCOLS = cols;
	signal(SIGWINCH, sigWindowSize);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void getTerminalCapabilities( void ) {
#define	TERMBUF_SIZE		2048
#define	TERMSBUF_SIZE		1024
#define	DEFAULT_TERM		"unknown"
	// Find out what kind of terminal this is.
	char *term;
	char termbuf[TERMBUF_SIZE];
	if ((term = getenv("TERM")) == NULL) term = (char *)DEFAULT_TERM;
	// Load information relative to the terminal from the erminfo database.
	switch (tgetent(termbuf, term)) {
	case -1:
		fatal("%s", "The terminfo database could not be found");
		break;
	case 0:
		fatal("%s", "There is no entry for this terminal in the terminfo database");
		break;
	default:
		break;
	}
	// Get various string-valued capabilities.
	static char sbuf[TERMSBUF_SIZE];
	char *sp = sbuf;
	if (NULL == (screeninit = tgetstr("ti", &sp)))
		fatal("%s", "There is no entry 'ti' (screen init) for this terminal in the terminfo database");
	if (NULL == (screendeinit = tgetstr("te", &sp)))
		fatal("%s", "There is no entry 'te' (screen deinit) for this terminal in the terminfo database");
	if (NULL == (clear = tgetstr("cl", &sp)))
		fatal("%s", "There is no entry 'cl' (clear screen) for this terminal in the terminfo database");
	if (NULL == (move = tgetstr("cm", &sp)))
		fatal("%s", "There is no entry 'cm' (cursor move) for this terminal in the terminfo database");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void setTerminalMode( terminal mode ) {
	static struct termios cooked, raw;
	switch (mode) {
	case MODE_ECHOED:
		if (0 != tcsetattr(STDIN_FILENO, TCSANOW, &cooked))
			fatal("%s", "Cannot set the parameter 'MODE_ECHOED' associated with the terminal");
		break;
	case MODE_RAW:
		if (0 != tcgetattr(STDIN_FILENO, &cooked))
			fatal("%s", "Cannot set the parameter 'MODE_RAW' associated with the terminal");
		raw = cooked;
		cfmakeraw(&raw);
		if (0 != tcsetattr(STDIN_FILENO, TCSANOW, &raw))
			fatal("%s", "Cannot set the parameter 'MODE_RAW/TCSANOW' associated with the terminal");
		break;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void clearBall( int iball ) {
	DISPLAY_AT(balls[iball].coordinates.line, balls[iball].coordinates.column, " ");
}

static void displayBall( int iball ) {
	DISPLAY_AT(balls[iball].coordinates.line, balls[iball].coordinates.column, START_COLOR "%s" STOP_COLOR, colors[iball], BALL);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int randomDraw( int max ) {
	// The randombytes_uniform() function returns an unpredictable value between 0 and MAX (excluded).
	return (int) randombytes_uniform((uint32_t)max);
}

static int rand_0_1( void ) {
	return (int) randombytes_random  () & 1;
}

static void initBall( int iball ) {
	balls[iball].steps.line = 1;
	balls[iball].steps.column = 1;

	int iborder = randomDraw(3); // between 0 and 3
	switch (iborder) {
	case 0:		// Top
		balls[iball].coordinates.line = 1;
		balls[iball].coordinates.column = randomDraw(nbCOLS) + 1;    // between 1 and nbCOLS
		balls[iball].direction = rand_0_1() ? SE : SW;
		break;
	case 1:		// Bottom
		balls[iball].coordinates.line = nbLINES;
		balls[iball].coordinates.column = randomDraw(nbCOLS) + 1;    // between 1 and nbCOLS
		balls[iball].direction = rand_0_1() ? NE : NW;
		break;
	case 2:		// Left
		balls[iball].coordinates.column = 1;
		balls[iball].coordinates.line = randomDraw(nbLINES) + 1;    // between 1 and nbLINES
		balls[iball].direction = rand_0_1() ? NE : SE;
		break;
	case 3:		// Right
		balls[iball].coordinates.column = nbCOLS;
		balls[iball].coordinates.line = randomDraw(nbLINES) + 1;    // between 1 and nbLINES;
		balls[iball].direction = rand_0_1() ? NW : SW;
		break;
	}
}

static void moveBall( int iball ) {
	switch (balls[iball].direction) {
	case NW:
		balls[iball].coordinates.line -= balls[iball].steps.line;
		balls[iball].coordinates.column -= balls[iball].steps.column;
		break;
	case SW:
		balls[iball].coordinates.line += balls[iball].steps.line;
		balls[iball].coordinates.column -= balls[iball].steps.column;
		break;
	case NE:
		balls[iball].coordinates.line -= balls[iball].steps.line;
		balls[iball].coordinates.column += balls[iball].steps.column;
		break;
	case SE:
		balls[iball].coordinates.line += balls[iball].steps.line;
		balls[iball].coordinates.column += balls[iball].steps.column;
		break;
	}
}

static void bounceBall( int iball ) {
	if (balls[iball].coordinates.line == 1) {
		if (balls[iball].coordinates.column == 1) {
			balls[iball].direction = SE;
		} else {
			if (balls[iball].coordinates.column == nbCOLS) {
				balls[iball].direction = SW;
			} else {
				switch (balls[iball].direction) {
				case NW:
					balls[iball].direction = SW;
					break;
				case NE:
					balls[iball].direction = SE;
					break;
				case SW:
				case SE:
					break;
				}
			}
		}
	} else {
		if (balls[iball].coordinates.line == nbLINES) {
			if (balls[iball].coordinates.column == 1) {
				balls[iball].direction = NE;
			} else {
				if (balls[iball].coordinates.column == nbCOLS) {
					balls[iball].direction = NE;
				} else {
					switch (balls[iball].direction) {
					case SW:
						balls[iball].direction = NW;
						break;
					case SE:
						balls[iball].direction = NE;
						break;
					case NW:
					case NE:
						break;
					}
				}
			}
		} else {
			if (balls[iball].coordinates.column == 1) {
				switch (balls[iball].direction) {
				case NW:
					balls[iball].direction = NE;
					break;
				case SW:
					balls[iball].direction = SE;
					break;
				case NE:
				case SE:
					break;
				}
			} else {
				if (balls[iball].coordinates.column == nbCOLS) {
					switch (balls[iball].direction) {
					case NE:
						balls[iball].direction = NW;
						break;
					case SE:
						balls[iball].direction = SW;
						break;
					case NW:
					case SW:
						break;
					}
				}
			}
		}
	}
	moveBall(iball);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void displayManual( void ) {
#define BASELINE	3
#define BASECOL		5
	DISPLAY_AT(BASELINE,     BASECOL, START_DIM_COLOR "╭──────────────────────────────────────────╮" STOP_COLOR, GREY);
	DISPLAY_AT(BASELINE + 1, BASECOL, START_DIM_COLOR "│ Type 'q' or 'Q' to quit the application. │" STOP_COLOR, GREY);
	DISPLAY_AT(BASELINE + 2, BASECOL, START_DIM_COLOR "│ Type '+' to add a ball (maximum %1d).      │." STOP_COLOR, GREY, MAXBALLS);
	DISPLAY_AT(BASELINE + 3, BASECOL, START_DIM_COLOR "│ Type '-' to delete a ball (minimum 1).   │" STOP_COLOR, GREY);
	DISPLAY_AT(BASELINE + 4, BASECOL, START_DIM_COLOR "╰──────────────────────────────────────────╯" STOP_COLOR, GREY);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#ifdef THREADS
void *threadBall( void *arg ) {
	int *numball = arg;
	int i = *numball - 1;
	cancelThreads[i] = false;
	// Ball structure initialization
	initBall(i);
	displayBall(i);
	usleep( (__useconds_t)WAIT);
	// First ball move
	clearBall(i);
	moveBall(i);
	displayBall(i);
	usleep( (__useconds_t)WAIT);
	// Go on
	while (true) {
		clearBall(i);
		if (cancelThreads[i]) break;
		bounceBall(i);
		displayBall(i);
		displayManual();
		usleep( (__useconds_t)WAIT);
	}
	pthread_exit(NULL);
	return NULL;
}
#endif
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void *threadKeyReader( void *arg ) {
	while (true) {
		switch (getchar()) {
		case 'q':
		case 'Q':
			restoreScreen();
			fprintf(stderr, START_COLOR "QUIT..." STOP_COLOR "\n", YELLOW);
			exit(EXIT_FAILURE);
			break;
		case '+':
			if (nbBALLS < 8) {
#ifdef THREADS
				int i;
				if (0 != (i = pthread_create(&threads[nbBALLS], NULL, threadBall, &nbBALLS)))
					fatal("Can't create bouncing ball thread #%d: [%s]", nbBALLS + 1, strerror(i));
				++nbBALLS;
#else
				++nbBALLS;
				initBall(nbBALLS - 1);
				displayBall(nbBALLS - 1);
				usleep((__useconds_t) (WAIT / nbBALLS));
				clearBall(nbBALLS - 1);
				moveBall(nbBALLS - 1);
				displayBall(nbBALLS - 1);
				usleep((__useconds_t) (WAIT / nbBALLS));
#endif
			} else
				DISPLAY_BELL();
			break;
		case '-':
			if (nbBALLS > 1) {
				--nbBALLS;
#ifdef THREADS
				cancelThreads[nbBALLS] = true;
#else
				clearBall(nbBALLS);
#endif
			} else
				DISPLAY_BELL();
			break;
		default:
			DISPLAY_BELL();
			break;
		}
	}
	// Never reached!!!
	pthread_exit(NULL);
	return NULL;
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_balls(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	cmdline_parser_balls_free(&args_info);
	if (sodium_init() < 0) {
		fatal("The SODIUM library couldn't be initialized");
	}
	//---- Signal processing  --------------------------------------------------
	signal(SIGWINCH, SIG_IGN);
	//---- Initialisations -----------------------------------------------------
	// Get terminal capabilities (instead of using escape code)
	setTerminalMode(MODE_RAW);
	getTerminalCapabilities();
	// Get terminal size
	getTerminalSize(&nbLINES, &nbCOLS);
	// Save initial state of the terminal screen to be restore on end
	SAVEINITSCREEN(nbLINES);
	// Change size signal management
	signal(SIGWINCH, sigWindowSize);
	CLEAR_ENTIRE_SCREEN();
	HIDE_CURSOR();
	//---- Go on ---------------------------------------------------------------
	// Initializations
	memset(balls, 0, sizeof(balls));
	nbBALLS = 1;
	int i;
	// Thread for keyboard reading
	pthread_t thread1;
	if (0 != (i = pthread_create(&thread1, NULL, threadKeyReader, NULL)))
		fatal("Can't create keyboard read thread: [%s]", strerror(i));
	displayManual();
#ifdef THREADS
	if (0 != (i = pthread_create(&threads[nbBALLS - 1], NULL, threadBall, &nbBALLS)))
		fatal("Can't create bouncing ball thread #%d: [%s]", nbBALLS + 1, strerror(i));
	while (true);
#else
	// Ball structure initialization
	for (i = 0; i < nbBALLS; i++) {
		initBall(i);
		displayBall(i);
		usleep((__useconds_t) (WAIT / nbBALLS));
	}
	// First ball move
	for (i = 0; i < nbBALLS; i++) {
		clearBall(i);
		moveBall(i);
		displayBall(i);
		usleep((__useconds_t) (WAIT / nbBALLS));
	}
	// Go on
	while (true) {
		for (i = 0; i < nbBALLS; i++) {
			clearBall(i);
			bounceBall(i);
			displayBall(i);
			displayManual();
			usleep((__useconds_t) (WAIT / nbBALLS));
		}
	}
#endif
	//---- Exit. Never reached!!! ---------------------------------------------
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
