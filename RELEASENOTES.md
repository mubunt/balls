# RELEASE NOTES: *balls*, Bouncing balls.

Functional limitations, if any, of this version are described in the *README.md* file.

**Version 1.0.0**:
  - First version.
